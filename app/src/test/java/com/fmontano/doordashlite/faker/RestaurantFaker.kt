package com.fmontano.doordashlite.faker

import com.fmontano.doordashlite.model.Menu
import com.fmontano.doordashlite.model.OpenHour
import com.fmontano.doordashlite.model.Restaurant
import com.fmontano.doordashlite.model.RestaurantDetail
import com.github.javafaker.Faker


object RestaurantFaker {

    private val faker = Faker()

    fun restaurantListItem(restaurantId: Int = faker.number().randomDigit()) {

        Restaurant(
                id = restaurantId,
                name = faker.lorem().words(2).joinToString(" "),
                description = faker.lorem().paragraph(),
                imageUrl = faker.internet().image(),
                deliveryFee = faker.number().randomDigit().toString(),
                averageRating = faker.number().digits(2).toFloat(),
                status = faker.lorem().word(),
                category = faker.lorem().word()
        )
    }

    fun restaurantDetail(restaurantId: Int = faker.number().randomDigit()) =
            RestaurantDetail(
                    id = restaurantId,
                    name = faker.lorem().words(2).joinToString(" "),
                    description = faker.lorem().paragraph(),
                    imageUrl = faker.internet().image(),
                    deliveryFee = faker.number().randomDigit().toString(),
                    averageRating = faker.number().digits(2).toFloat(),
                    status = faker.lorem().word(),
                    category = faker.lorem().word(),
                    numberOfRatings = faker.number().randomDigit(),
                    storeUrl = faker.internet().url(),
                    menus = listOf(menu(), menu()),
                    phoneNumber = faker.phoneNumber().toString(),
                    offersPickup = faker.bool().bool()
            )

    fun menu() = Menu(
            status = faker.lorem().word(),
            subtitle = faker.lorem().word(),
            name = faker.lordOfTheRings().character(),
            openHours = listOf(openHour(), openHour())
    )

    fun openHour() = OpenHour(
            dayOfWeek = faker.lorem().word(),
            openHour = faker.lorem().word(),
            closeHour = faker.lorem().word(),
            isToday = faker.bool().bool()
    )
}
