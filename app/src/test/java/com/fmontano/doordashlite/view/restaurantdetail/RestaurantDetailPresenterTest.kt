package com.fmontano.doordashlite.view.restaurantdetail

import android.content.res.Resources
import android.os.Bundle
import com.fmontano.doordashlite.BaseTest
import com.fmontano.doordashlite.R
import com.fmontano.doordashlite.faker.RestaurantFaker
import com.fmontano.doordashlite.model.RestaurantDetail
import com.fmontano.doordashlite.repository.RestaurantRepository
import io.reactivex.Single
import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito.*
import org.robolectric.util.ReflectionHelpers

class RestaurantDetailPresenterTest: BaseTest() {

    lateinit var presenter: RestaurantDetailPresenter
    @Mock lateinit var restaurantRepository: RestaurantRepository
    @Mock lateinit var view: RestaurantDetailContract.View
    @Mock lateinit var resources: Resources

    @Before
    fun setUp() {
        presenter = RestaurantDetailPresenter(restaurantRepository, resources)
        ReflectionHelpers.setField(presenter, "view", view)
    }

    @Test
    fun testInitialize_emptyBundle() {
        presenter.initialize(null)

        assertThat(ReflectionHelpers.getField<Int?>(presenter, "restaurantId")).isNull()
    }

    @Test
    fun testInitialize_validRestaurantId() {
        val restaurantId = faker.number().randomDigit()
        val bundle = Bundle().apply {
            putInt(RestaurantDetailContract.EXTRA_RESTAURANT_ID, restaurantId)
        }

        presenter.initialize(bundle)

        assertThat(ReflectionHelpers.getField<Int?>(presenter, "restaurantId")).isEqualTo(restaurantId)
    }

    @Test
    fun testOnBindView_failureWithErrorMessage() {
        val errorMessage = faker.lorem().word()
        val restaurantId = faker.number().randomDigit()
        ReflectionHelpers.setField(presenter, "restaurantId", restaurantId)
        `when`(restaurantRepository.restaurantDetail(restaurantId))
                .thenReturn(Single.error(Throwable(errorMessage)))

        presenter.onBindView()

        verify(view).showErrorMessage(R.string.error_title, errorMessage)
    }

    @Test
    fun testOnBindView_failureDefaultErrorMessage() {
        val restaurantId = faker.number().randomDigit()
        ReflectionHelpers.setField(presenter, "restaurantId", restaurantId)
        `when`(restaurantRepository.restaurantDetail(restaurantId))
                .thenReturn(Single.error(Throwable()))

        presenter.onBindView()

        verify(view).showErrorMessage(R.string.error_title, R.string.missing_restaurant_id_message)
    }

    @Test
    fun tesOnBindView_noRestaurantId() {
        presenter.onBindView()

        verify(view).showErrorMessage(R.string.error_title, R.string.missing_restaurant_id_message)
        verifyNoMoreInteractions(view)
    }

    @Test
    fun tesOnBindView_getRestaurantSuccess() {
        val restaurantId = faker.number().randomDigit()
        val restaurantDetail = RestaurantFaker.restaurantDetail(restaurantId)
        ReflectionHelpers.setField(presenter, "restaurantId", restaurantId)
        `when`(restaurantRepository.restaurantDetail(restaurantId))
                .thenReturn(Single.just(restaurantDetail))

        presenter.onBindView()

        assertThat(ReflectionHelpers.getField<RestaurantDetail>(presenter, "restaurantDetail"))
                .isEqualToComparingFieldByField(restaurantDetail)
        verify(view, never()).showErrorMessage(ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt())
        verify(view).showRestaurantImage(restaurantDetail.imageUrl)
        verify(view).setToolBarTitle(restaurantDetail.name)
        verify(view).showRestaurantName(restaurantDetail.name)
        verify(view).showAverageRating(restaurantDetail.averageRating)
        verify(view).showRatingCount(restaurantDetail.numberOfRatings)
    }

    @Test
    fun testOnMenuSelected() {
        val restaurantDetail = RestaurantFaker.restaurantDetail()
        ReflectionHelpers.setField(presenter, "restaurantDetail", restaurantDetail)
        val index = faker.number().numberBetween(0, restaurantDetail.menus.size)

        presenter.onMenuSelected(index)

        verify(view).showMenu(restaurantDetail.menus[index])
    }
}
