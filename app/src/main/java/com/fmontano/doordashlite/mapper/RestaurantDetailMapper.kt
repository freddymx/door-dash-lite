package com.fmontano.doordashlite.mapper

import com.fmontano.doordashlite.model.Menu
import com.fmontano.doordashlite.model.OpenHour
import com.fmontano.doordashlite.model.RestaurantDetail
import com.fmontano.doordashlite.net.data.MenuData
import com.fmontano.doordashlite.net.data.RestaurantData
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Mapper class that transform a raw [RestaurantData] received from the API and transforms
 * it into a [RestaurantDetail] instance
 */
@Singleton
class RestaurantDetailMapper @Inject constructor() {

    companion object {
        const val STORE_BASE_URL = "https://www.doordash.com/store/"
        val inputeFormatter: SimpleDateFormat = SimpleDateFormat("H:mm", Locale.getDefault())
        val outputFormatter: SimpleDateFormat = SimpleDateFormat("K:mm a", Locale.getDefault())
        val dayOfTheWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
    }

    fun transform(restaurantData: RestaurantData): RestaurantDetail =
            RestaurantDetail(
                    id = restaurantData.id,
                    name =  restaurantData.business.name,
                    description = restaurantData.description,
                    imageUrl = restaurantData.coverImgUrl,
                    deliveryFee = if (restaurantData.deliveryFee > 0) restaurantData.deliveryFee.toString() else "Free",
                    averageRating = restaurantData.averageRating,
                    status = restaurantData.status,
                    category = if (restaurantData.tags.isEmpty()) " - " else restaurantData.tags[0],
                    offersPickup = restaurantData.offersPickup ?: false,
                    menus = restaurantData.menus.map(this::transformMenuItem),
                    storeUrl = "$STORE_BASE_URL${restaurantData.slug}-${restaurantData.id}",
                    numberOfRatings = restaurantData.numberOfRatings,
                    phoneNumber = restaurantData.phoneNumber
            )

    private fun transformMenuItem(menuData: MenuData): Menu {
        val openHours = menuData.openHours
                .filterNot { it.isEmpty() }
                .mapIndexed { index, value ->
                    OpenHour(dayOfWeek = DateFormatSymbols.getInstance().weekdays[index + 1],
                            openHour = convertTo12Hours(value[0].hour, value[0].minute),
                            closeHour = convertTo12Hours(value[1].hour, value[1].minute),
                            isToday = index == dayOfTheWeek
                    )
                }

        return Menu(
                status = menuData.status,
                name = menuData.name,
                subtitle = menuData.subtitle,
                openHours = openHours
        )
    }

    private fun convertTo12Hours(hour: Int, minute: Int): String =
            outputFormatter.format(inputeFormatter.parse("$hour:$minute"))
}
