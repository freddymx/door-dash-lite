package com.fmontano.doordashlite.mapper

import com.fmontano.doordashlite.model.Restaurant
import com.fmontano.doordashlite.net.data.RestaurantData
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Mapper class that transform a raw [RestaurantData] received from the API and transforms
 * it into a [Restaurant] instance
 */
@Singleton
class RestaurantMapper @Inject constructor() {

    fun transform(restaurantDataList: List<RestaurantData>): List<Restaurant> =
            restaurantDataList.map { transform(it) }

    fun transform(restaurantData: RestaurantData): Restaurant =
            Restaurant(
                    id = restaurantData.id,
                    name =  restaurantData.business.name,
                    description = restaurantData.description,
                    imageUrl = restaurantData.coverImgUrl,
                    deliveryFee = if (restaurantData.deliveryFee > 0) restaurantData.deliveryFee.toString() else "Free",
                    averageRating = restaurantData.averageRating,
                    status = restaurantData.status,
                    category = if (restaurantData.tags.isEmpty()) " - " else restaurantData.tags[0]
            )
}
