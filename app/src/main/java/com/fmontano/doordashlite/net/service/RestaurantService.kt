package com.fmontano.doordashlite.net.service

import com.fmontano.doordashlite.net.data.RestaurantData
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RestaurantService {

    /**
     * Gets a list of restaurants for a given location
     *
     * @param lat Latitude of the location we are searching for
     * @param long Longitude of the location we are searching for
     * @param offset The current offset
     * @param limit Number of restaurants to fetch
     */
    @GET("restaurant")
    fun restaurants(@Query("lat") lat: Float,
                    @Query("lng") long: Float,
                    @Query("offset") offset: Int,
                    @Query("limit") limit: Int): Single<List<RestaurantData>>

    /**
     * Gets the details of a restaurant
     *
     * @param restaurantId The id of the restaurant
     */
    @GET("restaurant/{restaurantId}")
    fun restaurantDetails(@Path("restaurantId") restaurantId: Int): Single<RestaurantData>
}
