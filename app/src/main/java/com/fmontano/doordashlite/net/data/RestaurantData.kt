package com.fmontano.doordashlite.net.data

data class RestaurantData(val id: Int,
                          val description: String,
                          val phoneNumber: String?,
                          val yelpReviewCount: Int,
                          val maxCompositeScore: Int,
                          val averageRating: Float,
                          val deliveryRadius: Int,
                          val deliveryFee: Float,
                          val inflationRate: Int,
                          val tags: List<String>,
                          val menus: List<MenuData>,
                          val business: BusinessData,
                          val addressData: AddressData,
                          val priceRange: Int,
                          val isNewlyAdded: Boolean,
                          val offersPickup: Boolean?,
                          val coverImgUrl: String,
                          val serviceRate: Int,
                          val status: String,
                          val slug: String?,
                          val numberOfRatings: Int)

data class MenuData(val id: String,
                    val subtitle: String,
                    val name: String,
                    val status: String,
                    val isCatering: Boolean,
                    val popularItems: List<ItemData>,
                    val openHours: List<List<OpenHourData>>)

data class ItemData(val id: String,
                    val price: Int,
                    val description: String,
                    val imgUrl: String)

data class BusinessData(val id: Int,
                    val name: String)

data class AddressData(val city: String,
                       val state: String,
                       val street: String,
                       val lat: String,
                       val long: String,
                       val printableAddress: String)

data class OpenHourData(val hour: Int,
                        val minute: Int)
