package com.fmontano.doordashlite.app.dagger

import javax.inject.Scope

@Scope
@MustBeDocumented
annotation class ViewScope
