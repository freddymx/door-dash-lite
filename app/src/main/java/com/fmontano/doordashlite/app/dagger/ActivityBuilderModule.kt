package com.fmontano.doordashlite.app.dagger

import com.fmontano.doordashlite.view.restaurantdetail.RestaurantDetailActivity
import com.fmontano.doordashlite.view.restaurantdetail.RestaurantDetailModule
import com.fmontano.doordashlite.view.restaurantlist.RestaurantListActivity
import com.fmontano.doordashlite.view.restaurantlist.RestaurantListModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuildersModule {

    @ViewScope
    @ContributesAndroidInjector(modules = [RestaurantListModule::class])
    abstract fun bindRestaurantListActivity(): RestaurantListActivity

    @ViewScope
    @ContributesAndroidInjector(modules = [RestaurantDetailModule::class])
    abstract fun bindRestaurantDetailActivity(): RestaurantDetailActivity

}
