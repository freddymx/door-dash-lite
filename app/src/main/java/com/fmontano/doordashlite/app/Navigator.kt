package com.fmontano.doordashlite.app

import android.content.Intent
import android.net.Uri
import android.support.customtabs.CustomTabsIntent
import android.support.v4.content.ContextCompat
import com.fmontano.doordashlite.R
import com.fmontano.doordashlite.view.restaurantdetail.RestaurantDetailActivity
import com.fmontano.doordashlite.view.restaurantdetail.RestaurantDetailContract.Companion.EXTRA_RESTAURANT_ID
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Class responsible of handling the apps navigation
 */
@Singleton
class Navigator @Inject constructor() {

    /**
     * Opens the Restaurant Detail View
     *
     * @param view The view currently displayed
     * @param restaurantId ID of the restaurant to display
     */
    fun navigateToRestaurantDetailView(view: BaseContract.View, restaurantId: Int) {
        view.getHostActivity()?.let {
            val intent = Intent(it, RestaurantDetailActivity::class.java).apply {
                putExtra(EXTRA_RESTAURANT_ID, restaurantId)
            }
            it.startActivity(intent)
        }
    }

    /**
     * Opens a URL using chrome tabs when supported.
     *
     * @param view The view currently displayed
     * @param url The url to load
     */
    fun navigateToWebsite(view: BaseContract.View, url: String) {
        view.getHostActivity()?.let {
            val builder = CustomTabsIntent.Builder()
            builder.setToolbarColor(ContextCompat.getColor(it, R.color.colorPrimary))
            builder.addDefaultShareMenuItem()
            val customTabsIntent = builder.build()
            customTabsIntent.launchUrl(it, Uri.parse(url))
        }
    }

    /**
     * Opens a the phone's dialer with a number ready to call
     *
     * @param view The view currently displayed
     * @param phoneNumber The phone number to call
     */
    fun navigateToPhoneDialer(view: BaseContract.View, phoneNumber: String) {
        view.getHostActivity()?.let {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel: $phoneNumber")
            if (intent.resolveActivity(it.packageManager) != null) {
                it.startActivity(intent)
            }
        }
    }
}
