package com.fmontano.doordashlite.app

import android.app.Activity
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.annotation.StringRes

interface BaseContract {

    interface View {

        /**
         * Gets the layout for the view
         */
        @LayoutRes
        fun getViewResourceId(): Int?

        /**
         * Called when the view is ready to be displayed. This is called at the end of the
         * [Activity.onCreate] and before [Presenter.onBindView]
         */
        fun onViewLoaded(savedInstanceState: Bundle?)

        /**
         * Returns the host activity
         */
        fun getHostActivity(): Activity?

        /**
         * Sets the toolbar title
         *
         * @param title Title string
         */
        fun setToolBarTitle(title: String)

        /**
         * Sets the toolbar title
         *
         * @param titleResId Toolbar title resource id
         */
        fun setToolBarTitle(@StringRes titleResId: Int)

        /**
         * Sets the toolbar title
         */
        fun setDisplayHomeAsUpEnabled(enabled: Boolean)
    }

    interface Presenter<in V> {
        /**
         * Called when the activity. Used to get all the necessary data from the bundle
         * or savedInstanceState
         *
         * @param bundle The bundle
         * @param savedInstanceState The saved instance state, if any
         */
        fun initialize(bundle: Bundle?, savedInstanceState: Bundle? = null)

        /**
         * Called by the view right after [View.onViewLoaded] and is used to bind a view to
         * this presenter
         *
         * @param view The view to bind to this presenter
         */
        fun bindView(view: V)

        /**
         * Used to notify the presenter that the view is ready to display information
         */
        fun onBindView()

        /**
         * Called during [Activity.onStart]
         */
        fun onViewStart()

        /**
         * Called during [Activity.onResume]
         */
        fun onViewResumed()

        /**
         * Called during [Activity.onPause]
         */
        fun onViewPaused()

        /**
         * Called during [Activity.onStop]
         */
        fun onViewStopped()

        /**
         * Called during [Activity.onDestroy]
         */
        fun onViewDestroyed()
    }
}
