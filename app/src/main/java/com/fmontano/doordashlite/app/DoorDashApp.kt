package com.fmontano.doordashlite.app

import android.app.Activity
import android.app.Application
import com.fmontano.doordashlite.app.dagger.AppComponent
import com.fmontano.doordashlite.app.dagger.DaggerAppComponent
import com.fmontano.doordashlite.app.dagger.NetworkModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class DoorDashApp: Application(), HasActivityInjector {

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    private var appComponent: AppComponent? = null

    override fun activityInjector(): AndroidInjector<Activity> {
        return activityDispatchingAndroidInjector
    }

    override fun onCreate() {
        super.onCreate()

        val networkModule = NetworkModule.ApiModuleBuilder()
                .build()

        appComponent = DaggerAppComponent
                .builder()
                .application(this)
                .networkModule(networkModule)
                .build()

        appComponent?.inject(this)
    }
}
