package com.fmontano.doordashlite.app.dagger

import android.app.Application
import com.fmontano.doordashlite.app.DoorDashApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidInjectionModule::class,
    ActivityBuildersModule::class,
    NetworkModule::class,
    AppModule::class
])
interface AppComponent {

    fun inject(app: DoorDashApp)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun networkModule(networkModule: NetworkModule): Builder

        fun build(): AppComponent
    }
}
