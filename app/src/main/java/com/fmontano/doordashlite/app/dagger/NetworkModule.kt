package com.fmontano.doordashlite.app.dagger

import com.fmontano.doordashlite.BuildConfig
import com.fmontano.doordashlite.net.service.RestaurantService
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Singleton

@Module(includes = [AppModule::class])
class NetworkModule @Inject constructor(private val apiModuleBuilder: ApiModuleBuilder) {

    private val baseUrl = "https://api.doordash.com"
    private val apiVersion = "v2"

    class ApiModuleBuilder {

        fun build(): NetworkModule {
            return NetworkModule(this)
        }
    }

    @Provides
    @Singleton
    fun providesRetrofitClient(gson: Gson): Retrofit {
        val builder = OkHttpClient.Builder()

        if (BuildConfig.BUILD_TYPE == "debug") {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(loggingInterceptor)
        }
        val okHttpClient = builder.build()
        val retrofitBuilder = Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl("$baseUrl/$apiVersion/")
                .addConverterFactory(GsonConverterFactory.create(gson))
        retrofitBuilder.addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
        return retrofitBuilder.build()
    }

    @Provides
    @Singleton
    fun providesRestaurantService(retrofit: Retrofit): RestaurantService = retrofit.create(RestaurantService::class.java)
}
