package com.fmontano.doordashlite.app

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.include_toolbar.*
import javax.inject.Inject

abstract class BaseActivity<P: BaseContract.Presenter<V>, in V: BaseContract.View>: AppCompatActivity(),
        BaseContract.View {

    @Inject lateinit var presenter: P

    private var _glideRequests: GlideRequests? = null

    /**
     * Glide request used by views that load images inside adapters. This instance of
     * [GlideRequests] is lifecycle aware, so it will be stopped when [onStop] is called and
     * it will also return null this activity is finishing to avoid crashes when a service
     * call inside an adapter finished by the time the activity is finishing
     */
    val glideRequests: GlideRequests?
        get() {
            return if (isFinishing) {
                if (_glideRequests?.isPaused != true) {
                    _glideRequests?.onStop()
                }
                _glideRequests = null
                null
            } else {
                if (_glideRequests == null) {
                    _glideRequests = GlideApp.with(this)
                }
                _glideRequests
            }
        }

    @Suppress("UNCHECKED_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        getViewResourceId()?.let {
            setContentView(it)
        }

        presenter.initialize(intent.extras, savedInstanceState)

        toolbar?.let {
            setSupportActionBar(toolbar)
        }

        onViewLoaded(savedInstanceState)

        presenter.bindView(this as V)
    }

    override fun onResume() {
        super.onResume()

        presenter.onViewResumed()

        glideRequests?.resumeRequests()
    }

    override fun onStart() {
        super.onStart()

        presenter.onViewStart()
    }

    override fun onPause() {
        super.onPause()

        presenter.onViewPaused()
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.onViewDestroyed()
    }

    override fun onStop() {
        super.onStop()

        presenter.onViewStopped()

        glideRequests?.onStop()
    }

    override fun setToolBarTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun setToolBarTitle(titleResId: Int) {
        supportActionBar?.setTitle(titleResId)
    }

    override fun setDisplayHomeAsUpEnabled(enabled: Boolean) {
        supportActionBar?.setDisplayHomeAsUpEnabled(enabled)
    }

    override fun getHostActivity() = this
}
