package com.fmontano.doordashlite.app

import android.os.Bundle
import android.support.annotation.CallSuper
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

/**
 * Base presenter that takes care of binding a view to it as well as handle and disposing requests
 * when the view is stopped
 */
abstract class BasePresenter<V: BaseContract.View>: BaseContract.Presenter<V> {

    protected lateinit var view: V

    @Inject lateinit var navigator: Navigator

    private var _disposables: CompositeDisposable = CompositeDisposable()

    protected var disposables: CompositeDisposable = CompositeDisposable()
        get() {
            if (_disposables.isDisposed) {
                _disposables = CompositeDisposable()
            }
            return _disposables
        }

    final override fun bindView(view: V) {
        this.view = view
        onBindView()
    }

    @CallSuper
    override fun onViewStopped() {
        if (!_disposables.isDisposed) {
            _disposables.dispose()
        }
    }

    override fun initialize(bundle: Bundle?, savedInstanceState: Bundle?) {
        // Intentionally left empty to be overridden by child classes
    }

    override fun onBindView() {
        // Intentionally left empty to be overridden by child classes
    }

    override fun onViewResumed() {
        // Intentionally left empty to be overridden by child classes
    }

    override fun onViewPaused() {
        // Intentionally left empty to be overridden by child classes
    }

    override fun onViewDestroyed() {
        // Intentionally left empty to be overridden by child classes
    }

    override fun onViewStart() {
        // Intentionally left empty to be overridden by child classes
    }

}
