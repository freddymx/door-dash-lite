package com.fmontano.doordashlite.model

data class Restaurant(val id: Int,
                      val name: String,
                      val description: String,
                      val imageUrl: String,
                      val deliveryFee: String,
                      val averageRating: Float,
                      val status: String,
                      val category: String)

data class RestaurantDetail(val id: Int,
                            val name: String,
                            val description: String,
                            val imageUrl: String,
                            val deliveryFee: String,
                            val averageRating: Float,
                            val numberOfRatings: Int,
                            val status: String,
                            val category: String,
                            val offersPickup: Boolean,
                            val storeUrl: String,
                            val menus: List<Menu>,
                            val phoneNumber: String?)

data class Menu(val status: String,
                val subtitle: String?,
                val name: String,
                val openHours: List<OpenHour>)

data class OpenHour(val dayOfWeek: String,
                    val openHour: String,
                    val closeHour: String,
                    val isToday: Boolean)

