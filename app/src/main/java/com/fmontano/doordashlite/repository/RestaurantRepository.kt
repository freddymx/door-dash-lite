package com.fmontano.doordashlite.repository

import com.fmontano.doordashlite.mapper.RestaurantDetailMapper
import com.fmontano.doordashlite.mapper.RestaurantMapper
import com.fmontano.doordashlite.model.Restaurant
import com.fmontano.doordashlite.model.RestaurantDetail
import com.fmontano.doordashlite.net.service.RestaurantService
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository class that fetches results from the network and applies any operations to prepare the
 * data models for the presenters.
 *
 * @param restaurantService
 */
@Singleton
class RestaurantRepository @Inject constructor(private val restaurantService: RestaurantService,
                                               private val restaurantListMapper: RestaurantMapper,
                                               private val restaurantDetailMapper: RestaurantDetailMapper) {

    companion object {
        const val RESTAURANTS_PER_PAGE = 25
    }

    /**
     * Gets a list of restaurants for a given location
     *
     * @param lat Latitude of the location we are searching for
     * @param long Longitude of the location we are searching for
     * @param offset The current offset - defaults to 0
     * @param limit Number of restaurants to fetch - defaults to [RESTAURANTS_PER_PAGE]
     */
    fun restaurants(lat: Float, long: Float, offset: Int = 0, limit: Int = RESTAURANTS_PER_PAGE): Single<List<Restaurant>> =
            restaurantService.restaurants(lat, long, offset, limit)
                    .map(restaurantListMapper::transform)

    /**
     * Gets the details of a given restaurant
     *
     * @param restaurantId ID of the restaurant
     */
    fun restaurantDetail(restaurantId: Int): Single<RestaurantDetail> =
            restaurantService.restaurantDetails(restaurantId)
                    .map(restaurantDetailMapper::transform)

}
