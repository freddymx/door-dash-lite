package com.fmontano.doordashlite.view.restaurantlist

import dagger.Binds
import dagger.Module

@Module
interface RestaurantListModule {

    @Binds
    fun bindRestaurantListPresenter(restaurantListPresenter: RestaurantListPresenter): RestaurantListContract.Presenter
}
