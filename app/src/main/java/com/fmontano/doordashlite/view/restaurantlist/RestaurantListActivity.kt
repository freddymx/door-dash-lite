package com.fmontano.doordashlite.view.restaurantlist

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import com.fmontano.doordashlite.R
import com.fmontano.doordashlite.app.BaseActivity
import com.fmontano.doordashlite.model.Restaurant
import com.fmontano.doordashlite.utils.EndlessScrollListener
import kotlinx.android.synthetic.main.activity_restaurant_list.*

class RestaurantListActivity: BaseActivity<RestaurantListContract.Presenter, RestaurantListContract.View>(), RestaurantListContract.View {

    lateinit var restaurantListAdapter: RestaurantListAdapter
    private var doneLoading = false

    override fun getViewResourceId(): Int? = R.layout.activity_restaurant_list

    override fun onViewLoaded(savedInstanceState: Bundle?) {
        restaurantListAdapter = RestaurantListAdapter(glideRequests)
        recycler_view.apply {
            adapter = restaurantListAdapter
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@RestaurantListActivity)
            addItemDecoration(DividerItemDecoration(this@RestaurantListActivity, DividerItemDecoration.VERTICAL))
            addOnScrollListener(object : EndlessScrollListener(layoutManager = layoutManager) {

                override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                    if (!doneLoading) {
                        presenter.onLoadMore()
                    }
                }
            })
        }
    }

    override fun showRestaurants(restaurantList: List<Restaurant>) {
        restaurantListAdapter.addRestaurants(restaurantList)
        restaurantListAdapter.notifyDataSetChanged()
        restaurantListAdapter.setOnRestaurantClickListener(presenter::onRestaurantClick)
    }

    override fun showContent(show: Boolean) {
        toggleView(content, show)
    }

    override fun showLoading(show: Boolean) {
        toggleView(loading_progress_bar, show)
    }

    override fun doneLoading(doneLoading: Boolean) {
        this.doneLoading = doneLoading
    }

    override fun showLoadMoreIndicator(show: Boolean) {
        toggleView(load_more_indicator, show)
    }

    private fun toggleView(view: View, show: Boolean) {
        view.visibility = if (show) VISIBLE else GONE
    }
}
