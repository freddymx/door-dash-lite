package com.fmontano.doordashlite.view.restaurantdetail

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.fmontano.doordashlite.view.restaurantdetail.adapterdelegate.MenuHeaderAdapterDelegate
import com.fmontano.doordashlite.view.restaurantdetail.adapterdelegate.MenuItemAdapterDelegate
import com.hannesdorfmann.adapterdelegates3.AdapterDelegatesManager

class RestaurantDetailAdapter(private val items: List<*>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val delegatesManager = AdapterDelegatesManager<List<*>>()

    init {
        delegatesManager.addDelegate(MenuHeaderAdapterDelegate())
        delegatesManager.addDelegate(MenuItemAdapterDelegate())
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =
            delegatesManager.onBindViewHolder(items, position, holder)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
            delegatesManager.onCreateViewHolder(parent, viewType)

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int) =
            delegatesManager.getItemViewType(items, position)

}
