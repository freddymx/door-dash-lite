package com.fmontano.doordashlite.view.restaurantdetail.adapterdelegate

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.fmontano.doordashlite.R
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import kotlinx.android.synthetic.main.item_menu_name.view.*

class MenuHeaderAdapterDelegate: AdapterDelegate<List<*>>() {

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_menu_name, parent, false)
        return MenuHeaderViewHolder(view)
    }

    override fun isForViewType(items: List<*>, position: Int): Boolean =
            position == 0

    override fun onBindViewHolder(items: List<*>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
        (holder as MenuHeaderViewHolder).bind(items[position] as String)
    }

    class MenuHeaderViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val name = itemView.name

        fun bind(title: String) {
            name.text = title
        }
    }
}
