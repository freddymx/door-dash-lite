package com.fmontano.doordashlite.view.restaurantlist

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.fmontano.doordashlite.R
import com.fmontano.doordashlite.app.GlideRequests
import com.fmontano.doordashlite.model.Restaurant
import kotlinx.android.synthetic.main.item_restaurant.view.*

class RestaurantListAdapter(
        private val glideRequests: GlideRequests?
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val restaurantList = mutableListOf<Restaurant>()
    private var listener: (restaurantId: Int) -> Unit? = {}

    fun addRestaurants(restaurantList: List<Restaurant>) =
            this.restaurantList.addAll(this.restaurantList.size, restaurantList)

    fun setOnRestaurantClickListener(listener: (restaurantId: Int) -> Unit? = {}) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_restaurant, parent, false)
        return RestaurantViewHolder(view)
    }

    override fun getItemCount(): Int {
        return restaurantList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =
            (holder as RestaurantViewHolder).bind(restaurantList[position])

    inner class RestaurantViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        private val name: TextView = itemView.name
        private val info: TextView = itemView.info
        private val image: ImageView = itemView.image

        fun bind(restaurant: Restaurant) {
            name.text = restaurant.name
            info.text = itemView.context.getString(R.string.restaurant_info,
                    restaurant.category,
                    restaurant.deliveryFee,
                    restaurant.averageRating)

            if (glideRequests != null && !glideRequests.isPaused) {
                glideRequests.load(restaurant.imageUrl)
                        .into(image)
            }

            itemView.setOnClickListener {
                listener.invoke(restaurant.id)
            }
        }
    }
}
