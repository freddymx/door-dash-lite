package com.fmontano.doordashlite.view.restaurantlist

import com.fmontano.doordashlite.app.BasePresenter
import com.fmontano.doordashlite.app.dagger.ViewScope
import com.fmontano.doordashlite.model.Restaurant
import com.fmontano.doordashlite.repository.RestaurantRepository
import com.fmontano.doordashlite.repository.RestaurantRepository.Companion.RESTAURANTS_PER_PAGE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

@ViewScope
class RestaurantListPresenter @Inject constructor(
    private val restaurantRepository: RestaurantRepository
) : BasePresenter<RestaurantListContract.View>(),
    RestaurantListContract.Presenter {

    private var currentPage = 0

    override fun onBindView() {
        loadRestaurants()
    }

    private fun loadRestaurants(page: Int = 0) {
        if (page > 0) {
            view.showLoadMoreIndicator(true)
        } else {
            view.showLoading(true)
        }
        disposables += restaurantRepository.restaurants(lat = 37.422740f, long = -122.139956f, offset = page)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(onSuccess = this::onGetRestaurantSuccess, onError = this::onGetRestaurantFailure)
    }

    private fun onGetRestaurantSuccess(restaurantList: List<Restaurant>) {
        ++currentPage
        view.showLoading(false)
        view.showLoadMoreIndicator(false)
        view.showRestaurants(restaurantList)
        view.showContent(true)
        view.doneLoading(restaurantList.isEmpty() || restaurantList.size < RESTAURANTS_PER_PAGE)
    }

    private fun onGetRestaurantFailure(throwable: Throwable) {
        view.showLoadMoreIndicator(false)
        view.showLoading(false)
    }

    override fun onRestaurantClick(restaurantId: Int) {
        navigator.navigateToRestaurantDetailView(view, restaurantId)
    }

    override fun onLoadMore() {
        loadRestaurants(currentPage + 1)
    }
}
