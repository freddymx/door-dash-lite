package com.fmontano.doordashlite.view.restaurantdetail

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.AdapterView
import com.fmontano.doordashlite.R
import com.fmontano.doordashlite.app.BaseActivity
import com.fmontano.doordashlite.model.Menu
import kotlinx.android.synthetic.main.activity_restaurant_detail.*



class RestaurantDetailActivity: BaseActivity<RestaurantDetailContract.Presenter, RestaurantDetailContract.View>(),
        RestaurantDetailContract.View, AdapterView.OnItemSelectedListener {

    override fun getViewResourceId(): Int? = R.layout.activity_restaurant_detail

    override fun onViewLoaded(savedInstanceState: Bundle?) {
        setDisplayHomeAsUpEnabled(true)

        recycler_view.layoutManager = LinearLayoutManager(this)
        website.setOnClickListener { presenter.onWebsiteClick() }
        phone.setOnClickListener{ presenter.onPhoneClick() }
    }

    override fun showRestaurantImage(imageUrl: String) {
        glideRequests
                ?.load(imageUrl)
                ?.into(restaurant_image)
    }

    override fun showErrorMessage(title: Int, message: Int) {

    }

    override fun showErrorMessage(title: Int, message: String) {

    }

    override fun setMenus(menus: List<*>) {
        recycler_view.adapter = RestaurantDetailAdapter(menus)
    }


    override fun showRestaurantName(name: String) {
        this.name.text = name
    }

    override fun showAverageRating(averageRating: Float) {
        rating.rating = averageRating
    }

    override fun showRatingCount(reviewCount: Int) {
        review_count.text = resources.getQuantityString(R.plurals.review_count, reviewCount, reviewCount)
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        presenter.onMenuSelected(position)
    }

    override fun showMenu(menu: Menu) {
        /** Intentionally left empty */
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        /** Intentionally left empty */
    }
}
