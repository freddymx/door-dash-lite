package com.fmontano.doordashlite.view.restaurantdetail

import android.support.annotation.StringRes
import com.fmontano.doordashlite.app.BaseContract
import com.fmontano.doordashlite.model.Menu

interface RestaurantDetailContract {

    companion object {
        const val EXTRA_RESTAURANT_ID = "extraRestaurantId"
    }

    interface View: BaseContract.View {
        /**
         * Shows the restaurant's image
         *
         * @param imageUrl The url of the image to load
         */
        fun showRestaurantImage(imageUrl: String)

        /**
         * Shows an error message to the user
         *
         * @param title The resource id of the title
         * @param message The resource id of the message
         */
        fun showErrorMessage(@StringRes title: Int, @StringRes message: Int)

        /**
         * Shows an error message to the user
         *
         * @param title The resource id of the title
         * @param message The string message
         */
        fun showErrorMessage(@StringRes title: Int, message: String)

        /**
         * Shows the name of the restaurant
         *
         * @param name The name of the restaurant
         */
        fun showRestaurantName(name: String)

        /**
         * Shows the average rating of a restaurant
         *
         * @param averageRating The Average rating
         */
        fun showAverageRating(averageRating: Float)

        /**
         * Shows the rating of the restaurant
         *
         * @param reviewCount The review count
         */
        fun showRatingCount(reviewCount: Int)

        /**
         * Sets all the available menus
         *
         * @param menus List of menus to show
         */
        fun setMenus(menus: List<*>)

        /**
         * Shows the details or items of a menu
         */
        fun showMenu(menu: Menu)
    }

    interface Presenter: BaseContract.Presenter<View> {
        /**
         * Called when user taps on the visit website icon
         */
        fun onWebsiteClick()

        /**
         * Called when user taps on the phone icon
         */
        fun onPhoneClick()

        /**
         * Called when user taps on a menu
         */
        fun onMenuSelected(position: Int)
    }
}
