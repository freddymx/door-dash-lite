package com.fmontano.doordashlite.view.restaurantdetail

import android.content.res.Resources
import android.os.Bundle
import com.fmontano.doordashlite.R
import com.fmontano.doordashlite.app.BasePresenter
import com.fmontano.doordashlite.app.dagger.ViewScope
import com.fmontano.doordashlite.model.RestaurantDetail
import com.fmontano.doordashlite.repository.RestaurantRepository
import com.fmontano.doordashlite.view.restaurantdetail.RestaurantDetailContract.Companion.EXTRA_RESTAURANT_ID
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

@ViewScope
class RestaurantDetailPresenter @Inject constructor(
        private val restaurantRepository: RestaurantRepository,
        private val resources: Resources
): BasePresenter<RestaurantDetailContract.View>(), RestaurantDetailContract.Presenter {

    private var restaurantId: Int? = null
    private var restaurantDetail: RestaurantDetail? = null

    override fun initialize(bundle: Bundle?, savedInstanceState: Bundle?) {
        bundle?.let {
            restaurantId = bundle.getInt(EXTRA_RESTAURANT_ID)
        }
    }

    override fun onBindView() {
        restaurantId?.let {
            disposables += restaurantRepository.restaurantDetail(it)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy(
                            onSuccess = this::onGetRestaurantDetailsSuccess,
                            onError = this::onGetRestaurantDetailsFailure
                    )
        } ?: view.showErrorMessage(R.string.error_title, R.string.missing_restaurant_id_message)
    }

    private fun onGetRestaurantDetailsSuccess(restaurantDetail: RestaurantDetail) {
        this.restaurantDetail = restaurantDetail
        with(restaurantDetail) {
            view.showRestaurantImage(imageUrl)
            view.setToolBarTitle(name)
            view.showRestaurantName(name)
            view.showAverageRating(averageRating)
            view.showRatingCount(numberOfRatings)
            restaurantDetail.menus.let {
                val items = mutableListOf<Any>(resources.getString(R.string.available_menus))
                items.addAll(it)
                view.setMenus(items)
            }
        }
    }

    private fun onGetRestaurantDetailsFailure(throwable: Throwable) {
        throwable.message?.let {
            view.showErrorMessage(R.string.error_title, it)
        } ?: view.showErrorMessage(R.string.error_title, R.string.missing_restaurant_id_message)
    }

    override fun onMenuSelected(position: Int) {
        restaurantDetail?.let {
            view.showMenu(it.menus[position])
        }
    }

    override fun onWebsiteClick() {
        restaurantDetail?.storeUrl?.let {
            navigator.navigateToWebsite(view, it)
        }
    }

    override fun onPhoneClick() {
        restaurantDetail?.phoneNumber?.let{
            navigator.navigateToPhoneDialer(view, it)
        }
    }
}
