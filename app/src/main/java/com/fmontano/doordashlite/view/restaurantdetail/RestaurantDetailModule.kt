package com.fmontano.doordashlite.view.restaurantdetail

import com.fmontano.doordashlite.app.dagger.ViewScope
import dagger.Binds
import dagger.Module

@Module
interface RestaurantDetailModule {

    @Binds
    @ViewScope
    fun bindsRestaurantDetail(restaurantDetailPresenter: RestaurantDetailPresenter): RestaurantDetailContract.Presenter
}
