package com.fmontano.doordashlite.view.restaurantlist

import com.fmontano.doordashlite.app.BaseContract
import com.fmontano.doordashlite.model.Restaurant

interface RestaurantListContract {

    interface View: BaseContract.View {
        /**
         * Displays a list of restaurants
         *
         * @param restaurantList List of restaurants
         */
        fun showRestaurants(restaurantList: List<Restaurant>)

        /**
         * Notifies the view when there are not more restaurants to load
         *
         * @param doneLoading Whether or not the presenter is done loading data
         */
        fun doneLoading(doneLoading: Boolean)

        /**
         * Shows a loading more indicator
         *
         * @param show Whether to show or hide the view
         */
        fun showLoadMoreIndicator(show: Boolean)

        /**
         * Shows a loading indicator when loading initial set of restaurants
         *
         * @param show Whether to show or hide the view
         */
        fun showLoading(show: Boolean)

        /**
         * Shows the main content of the view
         *
         * @param show Whether to show or hide the view
         */
        fun showContent(show: Boolean)
    }

    interface Presenter: BaseContract.Presenter<View> {

        /**
         * Called when a user taps on a restaurant
         *
         * @param restaurantId Id of the restaurant tapped
         */
        fun onRestaurantClick(restaurantId: Int)

        /**
         * Called when the view is ready for more restaurants
         */
        fun onLoadMore()
    }
}
