package com.fmontano.doordashlite.view.restaurantdetail.adapterdelegate

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.fmontano.doordashlite.R
import com.fmontano.doordashlite.model.Menu
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import kotlinx.android.synthetic.main.item_menu_item_name.view.*

class MenuItemAdapterDelegate: AdapterDelegate<List<*>>() {

    private lateinit var layoutInflater: LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_menu_item_name, parent, false)
        return MenuItemViewHolder(view)
    }

    override fun isForViewType(items: List<*>, position: Int): Boolean =
            position > 0

    override fun onBindViewHolder(items: List<*>,
                                  position: Int,
                                  holder: RecyclerView.ViewHolder,
                                  payloads: MutableList<Any>) {
        (holder as MenuItemViewHolder).bind(items[position] as Menu)
    }

    inner class MenuItemViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        private val name = itemView.name
        private val chevron = itemView.chevron
        private val hoursContainer = itemView.hours_container
        private val nameContainer = itemView.name_container
        private val hoursOfOperationContainer = itemView.hours_container

        @SuppressLint("SetTextI18n")
        fun bind(menu: Menu) {

            this.name.text = menu.name

            nameContainer.setOnClickListener {
                toggleChevron()
            }

            hoursOfOperationContainer.removeAllViews()

            menu.openHours.map {
                val textView = layoutInflater.inflate(R.layout.item_hour_of_operation, hoursOfOperationContainer, false) as TextView
                textView.text = "${it.dayOfWeek}: ${it.openHour} - ${it.closeHour}"
                if (it.isToday) {
                    textView.setTypeface(null, Typeface.BOLD);
                }
                hoursOfOperationContainer.addView(textView)
            }
        }

        private fun toggleChevron() {
            val isExpanded = hoursContainer.visibility == View.VISIBLE
            hoursContainer.visibility = if (isExpanded) View.GONE else View.VISIBLE
            chevron.animate().rotationBy(if (isExpanded) 180f else -180f).start()
        }
    }
}
